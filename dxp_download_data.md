Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/01_operating_manual/S3-0012_C_BA_Gel_Pack_Support.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/01_operating_manual/S3-0012_C_OM_Gel_Pack_Support.pdf)                   |
| assembly drawing         |   [de](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/02_assembly_drawing/s3-0012_A_ZNB_gel-pak_support_uni.pdf)               |
| circuit diagram          | [de](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/03_circuit_diagram/S3-0012_B_EPLAN_Gel_Pack_Support.pdf)                 |
| maintenance instructions | [de](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/04_maintenance_instructions/S3-0012_B_WA_Gel_Pack_Support.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/04_maintenance_instructions/S3-0012_B_MI_Gel_Pack_Support.pdf)                  |
| spare parts              |  [de](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/05_spare_parts/S3-0012_S3-0013_C_EVL_Gel-Pack_Support.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0013-gel-pak-support-24/-/raw/main/05_spare_parts/S3-0012_S3-0013_C_SWP_Gel-Pack_Support.pdf)                 |

